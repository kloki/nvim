spinner_symbols = {
	"🙈 ",
	"🙉 ",
	"🙊 ",
	"🙈 ",
	"🙉 ",
	"🙊 ",
}
local function winnr()
	return vim.api.nvim_win_get_number(0)
end
mode_symbols = {
	["n"] = "",
	["no"] = "*",
	["nov"] = "*",
	["noV"] = "*",
	["no"] = "*",
	["niI"] = "*",
	["niR"] = "*",
	["niV"] = "*",
	["v"] = "",
	["V"] = "- ",
	[""] = " ",
	["s"] = "",
	["S"] = " -",
	[""] = " ",
	["i"] = "",
	["ic"] = "",
	["ix"] = "",
	["R"] = "屢",
	["Rc"] = "屢",
	["Rv"] = "屢",
	["Rx"] = "屢",
	["c"] = "",
	["cv"] = "",
	["ce"] = "",
	["r"] = "屢",
	["rm"] = "ﱟ",
	["r?"] = "",
	["!"] = "",
	["t"] = "",
	["nt"] = "",
}

local function vim_mode()
	local mode_code = vim.api.nvim_get_mode().mode
	if mode_symbols[mode_code] == nil then
		return mode_code
	end
	return mode_symbols[mode_code]
end

local function macro()
	local recording = vim.fn.reg_recording()

	if recording ~= "" then
		return "🔴 [" .. recording .. "]"
	end
	return ""
end
local function lsp_name()
	local msg = ""
	local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
	local clients = vim.lsp.get_active_clients()
	if next(clients) == nil then
		return msg
	end
	for _, client in ipairs(clients) do
		local filetypes = client.config.filetypes
		if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 and not string.find(msg, client.name) then
			msg = msg .. "[" .. client.name .. "]"
		end
	end
	return msg
end

local function scrollbar()
	local current_line = vim.fn.line(".")
	local total_lines = vim.fn.line("$")
	local chars = {
		"▱▱▱▱▱▱▱",
		"▰▱▱▱▱▱▱",
		"▰▰▱▱▱▱▱",
		"▰▰▰▱▱▱▱",
		"▰▰▰▰▱▱▱",
		"▰▰▰▰▰▱▱",
		"▰▰▰▰▰▰▱",
		"▰▰▰▰▰▰▰",
	}
	local line_ratio = current_line / total_lines
	local index = math.ceil(line_ratio * #chars)
	return chars[index]
end

seperators = {
	none = { left = "", right = "" },
	arrow = { left = "", right = "" },
	bubble = { left = "", right = "" },
	slant = { left = "", right = "" },
}
local gps = require("nvim-gps")
gps.setup({ disable_icons = true, separator = ":" })
require("lualine").setup({
	options = {
		theme = "dracula",
		icons_enabled = 0,
		section_separators = seperators["arrow"],
		component_separators = "",
		symbols = { modified = " ", readonly = " " },
	},
	sections = {
		lualine_a = { vim_mode },
		lualine_b = {
			{ "branch", icon = "" },
		},
		lualine_c = { macro },
		-- lualine_c = {
		--     {
		--         "filetype",
		--         icon_only = true,
		--         colored = true,
		--     },
		--     { "filename", path = 1 },
		--     { gps.get_location, cond = gps.is_available, icon = "" },
		-- },
		lualine_x = {
			{
				"diagnostics",
				sections = { "error", "warn", "hint" },
				diagnostics_color = {
					error = { fg = "#ff5555" },
					warn = { fg = "#ffb86c" },
					hint = { fg = "#8be9fd" },
				},
				symbols = { error = " ", warn = " ", suggestion = " " },
			},
			{
				"lsp_progress",
				display_components = { { "title", "percentage", "message" }, "spinner" },
				spinner_symbols = spinner_symbols,
			},
			{
				lsp_name,
				icon = "",
			},
		},
		lualine_y = {},
		lualine_z = {},
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { "filename" },
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	winbar = {
		lualine_b = {
			{
				"filetype",
				icon_only = true,
				colored = true,
			},
			{ "filename", path = 1 },
		},
		lualine_x = {
			{ gps.get_location, cond = gps.is_available, icon = "" },
		},
	},
	inactive_winbar = {
		lualine_c = {
			{
				"filetype",
				icon_only = true,
				colored = true,
			},
			{ "filename", path = 1 },
		},
		lualine_x = {
			{
				"diagnostics",
				sections = { "error", "warn", "hint" },
				diagnostics_color = {
					error = { fg = "#ff5555" },
					warn = { fg = "#ffb86c" },
					hint = { fg = "#8be9fd" },
				},
				symbols = { error = " ", warn = " ", suggestion = " " },
			},
		},
	},
})

local cmp = require("cmp")
local kind_map = {
	Text = "",
	Method = "",
	Function = "",
	Constructor = "",
	Field = "ﰠ",
	Variable = "",
	Class = "",
	Interface = "",
	Module = "",
	Property = "ﰠ",
	Unit = "塞",
	Value = "",
	Enum = "",
	Keyword = "",
	Snippet = "",
	Color = "",
	File = "",
	Reference = "",
	Folder = "",
	EnumMember = "",
	Constant = "",
	Struct = "",
	Event = "",
	Operator = "",
	TypeParameter = "",
}
local source_map = {
	nvim_lsp = " lsp",
	buffer = "buff",
	ultisnips = "snip",
	calc = "calc",
	path = "path",
	spell = "spell",
	emoji = "emoj",
	cmdline = "cmd",
}
local kind_colors = {
	Text = "#f8f8f2",
	Method = "#50fa7b",
	Function = "#50fa7b",
	Constructor = "#f8f8f2",
	Field = "#f8f8f2",
	Variable = "#ffb86c",
	Class = "#8be9fd",
	Interface = "#8be9fd",
	Module = "#ff5555",
	Property = "#f8f8f2",
	Unit = "#f8f8f2",
	Value = "#f8f8f2",
	Enum = "#f8f8f2",
	Keyword = "#8be9fd",
	Snippet = "#f8f8f2",
	Color = "#f8f8f2",
	File = "#f8f8f2",
	Reference = "#f8f8f2",
	Folder = "#f8f8f2",
	EnumMember = "#f8f8f2",
	Constant = "#bd93f9",
	Struct = "#8be9fd",
	Event = "#f8f8f2",
	Operator = "#f8f8f2",
	TypeParameter = "#f8f8f2",
}
cmp.setup({
	snippet = {
		expand = function(args)
			vim.fn["UltiSnips#Anon"](args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-Space>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.close(),
		["<C-y>"] = cmp.mapping.confirm({ select = true }),
	}),
	sources = {
		{ name = "nvim_lsp", max_item = 10 },
		{ name = "buffer", keyword_length = 4 },
		{ name = "ultisnips" },
		{ name = "calc" },
		{ name = "path" },
		{ name = "spell" },
		{ name = "emoji" },
	},
	formatting = {
		format = function(entry, vim_item)
			if entry.source.name == "nvim_lsp" then
				vim_item.kind = kind_map[vim_item.kind]
			else
				vim_item.kind = ""
			end

			vim_item.menu = "｢" .. source_map[entry.source.name] .. "｣"
			return vim_item
		end,
	},
	experimental = {
		native_menu = false,
	},
})
vim.api.nvim_set_hl(0, "CmpItemAbbr", { fg = "#bd93f9" })
vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", { fg = "#f8f8f2" })
vim.api.nvim_set_hl(0, "CmpItemKind", { fg = "#f8f8f2" })
for kind, color in pairs(kind_colors) do
	vim.api.nvim_set_hl(0, "CmpItemKind" .. kind, { fg = color })
end
vim.api.nvim_set_hl(0, "CmpItemMenu", { fg = "#f8f8f2" })

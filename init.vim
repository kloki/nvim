let g:python3_host_prog = '/home/koen/.config/nvim/python_host/bin/python'
set encoding=utf8
call plug#begin()
Plug 'lewis6991/impatient.nvim'
Plug 'dracula/vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'mhinz/vim-startify'
Plug 'nvim-lualine/lualine.nvim'

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-fugitive'


Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-telescope/telescope-project.nvim'
Plug 'xiyaowong/telescope-emoji.nvim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'nvim-treesitter/nvim-treesitter-refactor'

Plug 'SmiteshP/nvim-gps'
Plug 'arkav/lualine-lsp-progress'
Plug 'https://git.sr.ht/~whynothugo/lsp_lines.nvim'


Plug 'neovim/nvim-lspconfig'
Plug 'jose-elias-alvarez/null-ls.nvim'


Plug 'simrat39/rust-tools.nvim'
Plug 'mfussenegger/nvim-dap'

Plug 'svermeulen/vim-yoink'
Plug 'svermeulen/vim-subversive'

Plug 'lewis6991/gitsigns.nvim'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

Plug 'honza/vim-snippets', {'rtp': '.'}
Plug 'SirVer/ultisnips'

Plug 'vim-test/vim-test'
Plug 'kassio/neoterm'

Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-calc'
Plug 'hrsh7th/cmp-emoji'
Plug 'hrsh7th/cmp-nvim-lsp-document-symbol'
Plug 'quangnguyen30192/cmp-nvim-ultisnips'
Plug 'f3fora/cmp-spell'
call plug#end()

colorscheme dracula
set guifont=FantasqueSansMono\ Nerd\ Font:h16
set tgc
set pb=20
set winblend=20
set title
set inccommand=nosplit
let g:startify_lists = [{ 'type': 'files','header': ['   recent']}]
let g:startify_custom_header = map(split(system('toilet -f slant "Its baroque not minimalism"'), '\n'), '"   ". v:val')

set wildoptions=pum
set wim=longest:full
set shortmess+=c
set cmdheight=0

set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set smarttab
set expandtab
set nohlsearch
set scrolloff=5
set splitright
set hidden
set autoread

autocmd FileType go setlocal noexpandtab


set lazyredraw
set mouse=a
set mousemodel=popup

set clipboard+=unnamedplus
set wildignore=/home/koen/**/node_modules/**

"mappings
let g:mapleader="\<space>"
let g:maplocalleader="\\"

" text objects
onoremap io :exec "normal! ggVG"<cr>

"visual helpers
set listchars=tab:>·,trail:~,extends:>,precedes:<
set showbreak=↳
set list
set number
set colorcolumn=80
set noshowmode
set foldlevelstart=99
set foldmethod=indent
set signcolumn=yes:1

hi MatchParen cterm=underline,bold ctermbg=none


lua require("impatient")
lua require("treesitter-conf")
lua require("lsp-conf")
set completeopt=menu,menuone
lua require("completion-conf")
lua require("bar-conf")
lua require("telescope-conf")
lua require("gitsigns").setup({keymaps = {noremap=false}})
set laststatus=3
"terminal
tnoremap <C-o> <C-\><C-n>
au TermOpen * setlocal listchars= nonumber norelativenumber
let g:neoterm_autoscroll = 1
let g:neoterm_default_mod = "vertical"
set termguicolors
nmap gx <Plug>(neoterm-repl-send)
nmap gxx <Plug>(neoterm-repl-send)
xmap gx <Plug>(neoterm-repl-send)
augroup AutoAdjustResize
  autocmd!
  autocmd VimResized * execute "normal! \<C-w>="
augroup end

" history
if has("persistent_undo")
    set undodir=~/.undodir/
    set undofile
endif

"testing
nnoremap <leader>tt :TestNearest<CR>
nnoremap <leader>tf :TestFile<CR>
nnoremap <leader>ta :TestSuite<CR>
nnoremap <leader>tl :TestLast<CR>
nnoremap <leader>tv :TestVisit<CR>


let test#strategy= "neoterm"
let test#python#runner = 'pytest'

"snippets
" snippets are triggerd by nvm-cmp
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetsDir="~/.config/nvim/snips"
let g:UltiSnipsSnippetDirectories=["snips", "UltiSnips"]
let g:UltiSnipsExpandTrigger="<c-y>"

"yml
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"window movement

nnoremap <silent><C-Left> :wincmd h<cr>
nnoremap <silent><C-Right> :wincmd l<cr>
nnoremap <silent><C-Up> :wincmd k<cr>
nnoremap <silent><C-Down> :wincmd j<cr>

" copy pasting
nmap <c-n> <plug>(YoinkPostPasteSwapBack)
nmap <c-p> <plug>(YoinkPostPasteSwapForward)
nmap p <plug>(YoinkPaste_p)
nmap P <plug>(YoinkPaste_P)

nmap gp <plug>(YoinkPaste_gp)
nmap gP <plug>(YoinkPaste_gP)

nmap s <plug>(SubversiveSubstitute)
nmap ss <plug>(SubversiveSubstituteLine)
nmap S <plug>(SubversiveSubstituteToEndOfLin



"general
nnoremap <leader>w :w<CR>
nnoremap l <C-w><C-w>
nnoremap L <C-w>W
nnoremap <leader>h :split<CR>
nnoremap <leader>v :vsplit<CR>

xnoremap < <gv
xnoremap > >gv

nnoremap Y y$
nnoremap Q :bd<CR>
nnoremap gk yiwPa=<ESC>
nnoremap j o<ESC>

nnoremap n nzz
nnoremap N nzz
nnoremap J mjJ'j
vnoremap <silent><C-Down>  :m '>+1<CR>gv=gv
vnoremap <silent><C-Up>  :m '<-2<CR>gv=gv

"vimrc
nnoremap <leader>.e :e $MYVIMRC<CR>

nnoremap <leader>.r :source $MYVIMRC<CR>:lua vim.notify("Config file reloaded.")<CR>
nnoremap <leader>.r :e /home/koen/vale_styles/Vocab/Coding/accept.txt<CR>

" allow project config files
set exrc
set secure

